*Note: The text of this document has been taken from [the article](https://smashingmods.fandom.com/wiki/Knot126/Smash_Hit_Blender_Tools) at the smashing tech wiki.*

# Smash Hit Blender Tools

The '''Smash Hit Blender Tools''' are a few tools that help creating custom segments using blender. Please note that these tools are in an early beta phase. They may break if you don't do things a certain way.

## Install

To install, start blender and click ''Edit -> Preferences''. Once in the add-ons tab, press the install button near the top right. (It is next to refresh.) Browse to the file you just downloaded (the add-on) then open it. Once it has been installed, search for Smash Hit using the search field and enabled the add-on called ''Smash Hit Tools'' by checking the box. 

If you receive an error when trying to click on the checkbox, try renaming the file to something shorter, as the long file name seems to possibly be the cause. 

## Using the Tool

### Segment Properties

When installed, go to the top right of the of the 3D view and click the chevron. There will be some tabs that pop out, and one of these tabs will be titled ''Smash Hit''. Clicking on this tab will show two main sections: ''Segment Properties'' and ''Object Properties''. If you do not see the ''Smash Hit'' tab, try placing a cube down first, and then selecting it. Make sure you also go into the ''Tool'' tab and expand the ''Workspace'' dropdown. You should see a list of checkboxes, one of them being labeled ''Level Editing: Smash Hit Tools''. Make sure that is also checked.

Under segment properties, you will find some interesting settings: The first setting you may notice is ''Export using stone hack''. This will add the fake stone obstacle that will allow stone to appear even though Mesh files haven't been reverse engineered. 

Note: In the future, the stone hack may not be needed, and using blender to create stone this way means that almost no work will be needed to convert segments to using Mesh files when mesh file conversion is ready.

The second setting is called ''Color''. This controls the color of the fake stone, and may control the color of the Mesh in the future. The next setting is called Size. This will allow you to change the size of the segment. Generally, only the last number needs to be changed as this control how long the actual segment is. Lastly, there is the template property which controls the template the room uses.

### Object Properties
Object properties are a bit different. The panel contains the properties for the currently active object. The only common values are ''Object Type'', ''Hidden'', and ''Export object''. Object type controls the type of object this will be, like box, obstacle, power-up, decal, or water. The hidden mark will turn off or on the flag that sets the hidden property in the exported files. Finally, the export object setting will disable or enable the exporting of the object into the output XML file.

### Saving Segments
To save as a blender file, hit Control + S (or Control + Shift + S) like any other application. This will be needed if you want to come back to your work later.

To export a segment, go to ''File -> Export -> Smash Hit (.xml)''. Choose the location to save the file to, and click ''Export Smash Hit Segment''. The segment will be saved with the .xml file extension and can be loaded into the segments folder. The .mp3 extension will need to be added if modding for Android. It should also be mentioned that converting to XML is one way only and it can't be converted back into a blender file at this time.

Please note that the output XML file is not well formatted, so it will be hard to edit by hand.

### Additional Notes
When building out your segment, keep in mind that the camera (the player) will spawn one unit above the origin point (0, 0, 1) and will progress onto the negative X axis. Make sure your segment only exists in the negative X axis, and not the positive X axis. Also make sure that you move any ground you have placed below the origin point, as failing to do so will result in the camera hitting the floor right on its side. It's advised to have your blender scale to be around 1m to avoid massive scale issues

## Object Types
There are five types of objects: box (stone), obstacle, decal, power-up, and water. Please note that obstacles and power-ups must use their internal name when specifying what type to use.

### Box
For this type of object, make sure to use a cube, and make sure that the center point (orange dot when selected) of the cube is in the exact center of it. There is one special option for boxes: Reflective, which changes if reflections will be shown on the box. By default, they are disabled. This type will export a box and an associated stone obstacle if relevant.

:''In versions before 0.93, you would need to change the scale of the object using the item tab instead of using edit mode. This is no longer needed.''

### Obstacle
Note: Make sure to use an empty item for this and the rest of the object types. You can add empty items from ''Add -> Empty -> Plain Axes.''

The obstacle adds some settings for you to change. Firstly, type will control the type of obstacle that the current point is. The template parameter will control the template the obstacle will use. And lastly, the param[0 - 11] will allow editing and setting of all twelve parameters.

### Decal
Decals are things that occur in regular Smash Hit segments on a regular basis to add some effects like low hanging fog. Technically speaking, they are billboards. The decal setting allows changing the image that will be used. -4 to -1 are images for the door frames, and 0 to 63 are regular decals. The scale of the decal can be set by using the size property. Note that the last value will be ignored since these are 2D images.

### Power-up
Once again, these are fairly simple. The only field here is power-up, which allows setting the type of power-up to use.

### Water
There is only one option for water: size. Like the decal, this property only uses the first two numbers since water is two dimensional. (Actually, the sh_size property is used by both water and decal and switching water to a decal or the other way around should preserve the setting.)

## Stone Obstacle
The following stone obstacle is needed to make the stone hack work:

```lua
function init()
   cr, cg, cb = mgGetColor("color", 0.25, 0.5, 0.75)
   sizeX = mgGetFloat("sizeX", 0.5)
   sizeY = mgGetFloat("sizeY", 0.5)
   sizeZ = mgGetFloat("sizeZ", 0.5)
   raycast = mgGetBool("raycast", false)
   mgMaterial("steel")
   mgColor(cr, cg, cb)
   b = mgCreateBody(0, 0, 0)
   mgStatic(1)
   mgMask(0)
   mgCreateBox(b, sizeX, sizeY, sizeZ)
end

function tick()
end
```
